<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/criptomonedes">
        <cryptocoins>
            <xsl:for-each select="moneda">
                <coin>
                    <name>
                        <xsl:value-of select="nom"/>
                    </name>
                    <programming_language>
                        <xsl:value-of select="programat"/>
                    </programming_language>
                    <year>
                        <xsl:value-of select="any"/>
                    </year>
                    <author>
                        <xsl:value-of select="autor"/>
                    </author>
                    <conversion>
                        <dollars>
                            <xsl:value-of select="conversio/dolars"/>
                        </dollars>
                        <euro>
                            <xsl:value-of select="conversio/euro"/>
                        </euro>
                        <yen>
                            <xsl:value-of select="conversio/yen"/>
                        </yen>
                        <pounds>
                            <xsl:value-of select="conversio/libra"/>
                        </pounds>
                    </conversion>
                    <website>
                        <xsl:value-of select="link"/>
                    </website>
                    <description>
                        <xsl:value-of select="descripcio"/>
                    </description>
                </coin>
            </xsl:for-each>
        </cryptocoins>
    </xsl:template>

</xsl:stylesheet>