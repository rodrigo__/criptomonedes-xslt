<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/criptomonedes">
        <criptodivisa>
            <xsl:for-each select="moneda">
                <divisa>
                    <creador>
                        <xsl:value-of select="autor"/>
                    </creador>
                    <nom>
                        <xsl:value-of select="nom"/>
                    </nom>
                    <descripcio_curta>
                        <xsl:value-of select="descripcio"/>
                    </descripcio_curta>
                    <conversio_euro>
                        <xsl:value-of select="conversio/euro"/>
                    </conversio_euro>
                </divisa>
            </xsl:for-each>
        </criptodivisa>
    </xsl:template>

</xsl:stylesheet>