<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">

        <estadistica_web>

            <xsl:attribute name="_id">
                <xsl:value-of select="@_id"></xsl:value-of>
            </xsl:attribute>

            <xsl:for-each select="response/row/row">

                <xsl:if test="taxa_de_rebot=44.25"> <!-- aqui es buscar per rebots -->

                    <ping_rebots>
                        <xsl:value-of select="taxa_de_rebot"/>
                    </ping_rebots>

                    <nom_web>
                        <xsl:value-of select="web"/>
                    </nom_web>

                    <link>
                        <xsl:value-of select="url"/>
                    </link>

                    <año>
                        <xsl:value-of select="any"/>
                    </año>

                    <xsl:copy-of select="p_gines_visualitzades"/>

                    <usuaris_sessions>
                        <usuaris_conectats>
                            <xsl:value-of select="usuaris"></xsl:value-of>
                        </usuaris_conectats>
                        <sessions_obertes>
                            <xsl:value-of select="sessions"></xsl:value-of>
                        </sessions_obertes>
                    </usuaris_sessions>
                    <nimuts_de_activacio>
                        <xsl:value-of select="durada_mitjana_de_la_sessi_en_minuts"/>
                    </nimuts_de_activacio>
                    <xsl:for-each select="response/row/row"><!--aqui filta per durada de la cessio  -->
                        <xsl:sort data-type="number"/>
                        <xsl:value-of select="durada_mitjana_de_la_sessi_en_minuts"/>
                    </xsl:for-each>

                </xsl:if>

            </xsl:for-each>


        </estadistica_web>


    </xsl:template>

</xsl:stylesheet>
