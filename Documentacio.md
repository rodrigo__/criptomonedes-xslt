﻿#  AA.2.2.3. Avançat XSLT

## Part 1
Hem triat l'XML del **Servei Meteorològic de Catalunya** dels quals té informació d'usuaris connectats, pàgines visualitzades, etc... sobre les pàgines que tenen els servidors del SMC.

##### Codi XML:
![codixml](https://gitlab.com/rodrigo__/fotos/raw/master/part1_smc_xml.PNG)
> És més gran, però la captura és petita per fer d'exemple

##### Resultat de transformació amb XSLT:
![enter image description here](https://gitlab.com/rodrigo__/fotos/raw/master/part1_smc_res.PNG)

## Part 2
Els XML de la pràctica de la UF1 va sobre criptomonedes. Ara és mostrarà com hem intentat transformar-lo segons l'indicat al enunciat.

**Versió 1: Traduir-lo del idioma original al anglès:**
##### Codi XML
![xml1](https://gitlab.com/rodrigo__/fotos/raw/master/part2_trans_codi.PNG)

> És més gran, però la captura és petita per fer d'exemple

##### Codi XSLT
![xslt1](https://gitlab.com/rodrigo__/fotos/raw/master/part2_angles_codi.PNG)

##### Resultat
![res1](https://gitlab.com/rodrigo__/fotos/raw/master/part2_trans_res.PNG)

**Versió 2: Transformar-lo a un criteri propi:**
##### Codi XML
![xml2](https://gitlab.com/rodrigo__/fotos/raw/master/part2_trans_codi.PNG)
##### Codi XSLT
![xslt2](https://gitlab.com/rodrigo__/fotos/raw/master/part2_trans2_codi.PNG)
##### Resultat
![res2](https://gitlab.com/rodrigo__/fotos/raw/master/part2_trans2_res.PNG)
